package vorgabeA;

import java.util.Scanner;

public class TestPrimzahl {

	private static Scanner scan;

	public static void main(String[] args) {
	
		scan = new Scanner(System.in);
		Stoppuhr stopp = new Stoppuhr();
		while (true) {
		System.out.print("Zahl bitte eingeben: ");
			long primzahl = scan.nextLong();
			
			stopp.start();
			System.out.println("====================================================================");
			boolean isPrim = Primzahl.isPrim(primzahl);
			System.out.println("Zahl " +"["+ primzahl + "] ist = " + isPrim);
			System.out.println("====================================================================");
			stopp.stopp();
			System.out.println("Die Zeit betrug: " + stopp.getSeconds() + " Sekunde(n) und " + stopp.getMillis() + " Millisekunde(n).");
			System.out.println("====================================================================");
		}
		
	
	
	}
}

