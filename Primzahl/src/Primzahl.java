import java.util.Scanner;

public class Primzahl {
	
	public static boolean istPrim(long zahl) {
		if (zahl == 0 || zahl == 1) 								
			return false;					
		for (int i = 2; i < zahl; i++) {							
			if (zahl % i == 0)						
				return false;
		}															
		return true;
}

	public static void main(String[] args) {
			
		Scanner scan = new Scanner(System.in);
			long zahl;
			System.out.print("Zahl eingeben: ");
			zahl = scan.nextLong();					
			if (istPrim(zahl))										
				System.out.println(zahl + " ist eine Primzahl.");
			else
				System.out.println(zahl + " ist keine Primzahl.");
		} 
	}