package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();
			if (fuellstand <= 95)
				fuellstand = fuellstand + 5;
			else {
				if (fuellstand == 99)
					fuellstand = fuellstand + 1;
				if (fuellstand == 98)
					fuellstand = fuellstand + 2;
				if (fuellstand == 97)
					fuellstand = fuellstand + 3;
				if (fuellstand == 96)
					fuellstand = fuellstand + 4;
			}
			

			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText("" + fuellstand + "%");
		}
		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			if (fuellstand >= 2)
				fuellstand = fuellstand - 2;
			else {
				if (fuellstand == 1)
					fuellstand = fuellstand - 1;
			}

			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText("" + fuellstand + "%");
		}
		if (obj == f.btnZurucksetzen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText("" + fuellstand + "%");
		}
	}
}