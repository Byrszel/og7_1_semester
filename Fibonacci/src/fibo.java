import java.util.Scanner;

public class fibo {

	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Eingabe: ");
		int x;
		x = scan.nextInt();
		int[] zahl = new int[x];
		
		
		for (int i = 0; i < zahl.length; i++)
		System.out.println(fib(i));
		
	}
	
	
	
	public static int fib(int n) {
		   
		
		if(n == 0) {
		     return 0;
		   } else if (n == 1) {
		     return 1;
		   } else {
		      return fib(n-1) + fib(n-2);
		   }
		}
		
}
