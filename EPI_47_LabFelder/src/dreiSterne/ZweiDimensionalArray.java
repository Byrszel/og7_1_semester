package dreiSterne;

import java.util.Scanner;

public class ZweiDimensionalArray {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);

		int x;
		System.out.print("Bitte die Anzahl der Felder eingeben: ");
		x = scan.nextInt();
		int Array[][] = new int[x][x];
		
		for (int i = 0 ; i < x; i++) {
			for (int j = 0; j < x;j++) {
				Array[i][j]=i*j;
				if (j < x-1) {
					System.out.print(Array[i][j]+",");
				}	
				else	{
					System.out.println(Array[i][j]);
				}
			
		
		scan.close();
		
			}
		}
	}
}
