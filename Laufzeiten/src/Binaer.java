import java.util.Scanner;

public class Binaer {

	private final static int NICHT_GEFUNDEN = -1;

	public static int suche(long[] liste, long gesuchteZahl, int start, int end) {
		int x = (start + end) / 2;
		if (start > end) {
			return NICHT_GEFUNDEN;
		} else if (liste[x] == gesuchteZahl) {
			return x;
		} else if (liste[x] > gesuchteZahl) {
			return suche(liste, gesuchteZahl, start, x - 1);
		} else if (liste[x] < gesuchteZahl) {
			return suche(liste, gesuchteZahl, x + 1, end);
		}	
		return NICHT_GEFUNDEN;
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		long[] liste1 = SortedListGenerator.getSortedList(15000000); //Anzahl angeben
		while (true) {
		long zahl;
		System.out.print("Zahl: ");
		zahl = scan.nextLong();
		long zeit = System.currentTimeMillis();
		if (Binaer.suche(liste1, zahl, 0, liste1.length)!= NICHT_GEFUNDEN)
			System.out.println(Binaer.suche(liste1, zahl, 0, liste1.length) + "ter Stelle - " + (System.currentTimeMillis() - zeit + "ms"));
			else {
				System.out.println("Nicht vorhanden");
			}
		}
	}
}



