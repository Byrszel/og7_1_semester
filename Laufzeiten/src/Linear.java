import java.util.Scanner;

public class Linear {

	private final static long NICHT_GEFUNDEN = -1;

	public static long suche(long[] zahlen, long gesuchteZahl) {
		for (int j = 0; j < zahlen.length; j++)
			if (zahlen[j] == gesuchteZahl)
				return j;
		return NICHT_GEFUNDEN;
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		long[] liste1 = SortedListGenerator.getSortedList(15000000); //Anzahl angeben
		while (true) {
		long zahl;
		System.out.print("Zahl: ");
		zahl = scan.nextLong();
		long zeit = System.currentTimeMillis();
		if (Linear.suche(liste1, zahl)!= NICHT_GEFUNDEN)
		System.out.println(Linear.suche(liste1, zahl) + "ter Stelle - " + (System.currentTimeMillis() - zeit + "ms"));
		else {
			System.out.println("Nicht vorhanden");
		}
		}
	}
}
