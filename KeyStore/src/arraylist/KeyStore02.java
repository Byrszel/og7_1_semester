package arraylist;

import java.util.ArrayList;

public class KeyStore02 {

	private ArrayList<String> key;

	public KeyStore02() {
		key = new ArrayList<String>();
	}

	public String get(int index) {
		return key.get(index);
	}

	public int size() {
		return key.size();
	}

	public void clear() {
		key.clear();
	}
	
	

	public String toString() { // 4
		String erg = "[ "; // z�hlt jede position ab und druckt sie
		for (int i = 0; i < key.size(); i++)
			erg = erg + key.get(i) + " ";
		erg += "]";
		return erg;
	}

	public boolean add(String eintrag) {
		return key.add(eintrag);
	}

	public int indexOf(String eintrag) {
		return key.indexOf(eintrag);
	}

	public void remove(int index) {
		key.remove(index);
		
	}

}