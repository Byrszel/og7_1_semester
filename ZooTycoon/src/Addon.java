
public class Addon {
	
	private int idnummer;
	private String bezeichnung;
	private double verkaufspreis;
	private int bestandmax;
	private int bestandspieler;
	
	public Addon(int idnummer, String bezeichnung, double verkaufspreis, int bestandmax, int bestandspieler) {
		super();
		this.idnummer = idnummer;
		this.bezeichnung = bezeichnung;
		this.verkaufspreis = verkaufspreis;
		this.bestandmax = bestandmax;
		this.bestandspieler = bestandspieler;
	}


	public int getIdnummer() {
		return idnummer;
	}



	public void setIdnummer(int idnummer) {
		this.idnummer = idnummer;
	}



	public String getBezeichnung() {
		return bezeichnung;
	}



	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}



	public double getVerkaufspreis() {
		return verkaufspreis;
	}



	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}



	public int getBestandmax() {
		return bestandmax;
	}



	public void setBestandmax(int bestandmax) {
		this.bestandmax = bestandmax;
	}



	public int getBestandspieler() {
		return bestandspieler;
	}



	public void setBestandspieler(int bestandspieler) {
		this.bestandspieler = bestandspieler;
	}



	@Override
	public String toString() {
		return "Addon [idnummer=" + idnummer + ", bezeichnung=" + bezeichnung + ", verkaufspreis=" + verkaufspreis
				+ ", bestandmax=" + bestandmax + ", bestandspieler=" + bestandspieler + "]";
	}


	public static void main(String[] args) {
		System.out.println(".");
	}

}
