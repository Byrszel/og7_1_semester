package collection_test;

import java.util.*;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) loeschen");
		System.out.println(" 4) Die groesste ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			
			case '1':	
				
                System.out.println("\nSie haben die Auswahl 'eintragen' ausgewählt.");
                
                System.out.print("Autor: ");
                String autor = myScanner.next();
                
                System.out.print("Titel: ");
                String titel = myScanner.next();
                
                System.out.print("ISBN: ");
                String isbn = myScanner.next();
                
                Buch newBook = new Buch(autor, titel, isbn);
                buchliste.add(newBook);
                
                System.out.println("\nBuch wurde hingefügt.");
                break;
                
			case '2':
				
				System.out.println("\nSie haben die Auswahl 'finden' ausgewählt.");
				System.out.print("ISBN bitte eingeben: ");
				String isbn1 = myScanner.next();
				System.out.println(findeBuch(buchliste,isbn1).toString());
				break;
				
						
			case '3':
				
				System.out.println("\nUm ein Buch zu löschen, bitte geben Sie die ISBN ein: ");
				String isbn2 = myScanner.next();
				loescheBuch(buchliste, isbn2);
				break;
				
			case '4':
				
				System.out.println(ermitteleGroessteISBN(buchliste));
				break;	
				
			case '5':
				
				System.out.println("\nZeige Buchliste:");
				System.out.println(buchliste.toString());
				break;
				
			case '9':
				
				System.exit(0);
				break;
				
			default:
				menue();
				wahl = myScanner.next().charAt(0);
		} // switch

	} while (wahl != 9);
}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		for (int i = 0; i < buchliste.size(); i++)
			if (buchliste.get(i).getIsbn().equals(isbn))
				return buchliste.get(i);
		return null;
	}

	public static boolean loescheBuch(List<Buch> buchliste, String isbn) {
		 return buchliste.remove(findeBuch(buchliste, isbn));
	}

	public static String ermitteleGroessteISBN(List<Buch> buchliste) {
		String isbn = "";
		for (int i = 0; i < buchliste.size(); i++) {
			if (buchliste.get(i).getIsbn().compareTo(isbn) > 0) {
				isbn = buchliste.get(i).getIsbn();
			}
		}
		return isbn;	
	}

}