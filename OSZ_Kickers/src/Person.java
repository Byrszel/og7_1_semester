
public class Person {
	
	private String name;
	private int telefonnummer;
	private boolean jahresbetrag;
	

	public Person(String name, int telefonnummer, boolean jahresbetrag) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbetrag = jahresbetrag;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean isJahresbetrag() {
		return jahresbetrag;
	}

	public void setJahresbetrag(boolean jahresbetrag) {
		this.jahresbetrag = jahresbetrag;
	}

}
