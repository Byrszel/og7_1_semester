
public class Schiedsrichter extends Person {
	
	private int anzahlGepfiffeneSpiele;
	
	public Schiedsrichter(String name, int telefonnummer, boolean jahresbetrag, int anzahlGepfiffeneSpiele) {
		super(name, telefonnummer, jahresbetrag);
		this.anzahlGepfiffeneSpiele = anzahlGepfiffeneSpiele;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public int getAnzahlGepfiffeneSpiele() {
		return anzahlGepfiffeneSpiele;
	}

	public void setAnzahlGepfiffeneSpiele(int anzahlGepfiffeneSpiele) {
		this.anzahlGepfiffeneSpiele = anzahlGepfiffeneSpiele;
	}

}
