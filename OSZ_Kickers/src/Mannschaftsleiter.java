
public class Mannschaftsleiter extends Person {
	
	private String mannschaftsname;
	private int rabattJahresbetrag;
	
	public Mannschaftsleiter(String name, int telefonnummer, boolean jahresbetrag, String mannschaftsname,
			int rabattJahresbetrag) {
		super(name, telefonnummer, jahresbetrag);
		this.mannschaftsname = mannschaftsname;
		this.rabattJahresbetrag = rabattJahresbetrag;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getMannschaftsname() {
		return mannschaftsname;
	}

	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}

	public int getRabattJahresbetrag() {
		return rabattJahresbetrag;
	}

	public void setRabattJahresbetrag(int rabattJahresbetrag) {
		this.rabattJahresbetrag = rabattJahresbetrag;
	}

}
