
public class TestKickers {

	public static void main(String[] args) {
		
		Person p1 = new Person(null, 0, false);
		p1.setName("Fred");
		p1.setTelefonnummer(49162934);
		p1.setJahresbetrag(false);
		System.out.println("Person hei�t: " + p1.getName());
		System.out.println(p1.getName() + " hat die Telefonnummer: " + p1.getTelefonnummer());
		System.out.println(p1.getName() + " den Jahresbetrag bezahlt: " + p1.isJahresbetrag());
		System.out.println("================================================================");
		
		Spieler s1 = new Spieler(null, 0, false, 0, null);
		s1.setName("Hanz");
		s1.setTelefonnummer(5491902);
		s1.setJahresbetrag(true);
		s1.setSpielposition("St�rmer");
		s1.setTrikotnummer(33);
		System.out.println("Spieler hei�t: " + s1.getName());
		System.out.println(s1.getName() + " hat die Telefonnummer: " + s1.getTelefonnummer());
		System.out.println(s1.getName() + " hat den Jahresbetrag bezahlt: " + s1.isJahresbetrag());
		System.out.println(s1.getName() + " spielt die Position: " + s1.getSpielposition());
		System.out.println(s1.getName() + " tr�gt die Trikotnummer: " + s1.getTrikotnummer());
		System.out.println("================================================================");
		
		Mannschaftsleiter m1 = new Mannschaftsleiter(null, 0, false, null, 0);
		m1.setName("Derek");
		m1.setTelefonnummer(48194155);
		m1.setJahresbetrag(true);
		m1.setMannschaftsname("OSZ Kickers TORnado 1996");
		m1.setRabattJahresbetrag(50);
		System.out.println("Mannschaftsleiter hei�t: " + m1.getName());
		System.out.println(m1.getName() + " hat die Telefonnummer: " + m1.getTelefonnummer());
		System.out.println(m1.getName() + " hat den Jahresbetrag bezahlt: " + m1.isJahresbetrag());
		System.out.println(m1.getName() + "'s Mannschaft hei�t: " + m1.getMannschaftsname());
		System.out.println(m1.getName() + " hat einen Jahresbetrag von: " + m1.getRabattJahresbetrag() + "%");
		System.out.println("================================================================");
		
		Trainer t1 = new Trainer(null, 0, false, null, 0);
		t1.setName("Samuel");
		t1.setTelefonnummer(87591053);
		t1.setJahresbetrag(false);
		t1.setLizenzklasse("C");
		t1.setAufwandentschaedigung(450);
		System.out.println("Trainer hei�t: " + t1.getName());
		System.out.println(t1.getName() + " hat die Telefonnummer: " + t1.getTelefonnummer());
		System.out.println(t1.getName() + " hat den Jahresbetrag bezahlt: " + t1.isJahresbetrag());
		System.out.println(t1.getName() + " hat die Lizenzklasse: " + t1.getLizenzklasse());
		System.out.println(t1.getName() + " erh�lt eine Aufwandsentschaedigung von: " + t1.getAufwandentschaedigung());
		System.out.println("================================================================");
		
		Schiedsrichter r1 = new Schiedsrichter(null, 0, false, 0);
		r1.setName("Willy");
		r1.setTelefonnummer(15906863);
		r1.setJahresbetrag(false);
		r1.setAnzahlGepfiffeneSpiele(15);
		System.out.println("Trainer hei�t: " + r1.getName());
		System.out.println(r1.getName() + " hat die Telefonnummer: " + r1.getTelefonnummer());
		System.out.println(r1.getName() + " hat den Jahresbetrag bezahlt: " + r1.isJahresbetrag());
		System.out.println(r1.getName() + " hat bereits " + r1.getAnzahlGepfiffeneSpiele() + " Spiele abgepfiffen.");
		
	}

}
