
public class Spieler extends Person {
	
	private int trikotnummer;
	private String spielposition;

	
	public Spieler(String name, int telefonnummer, boolean jahresbetrag, int trikotnummer, String spielposition) {
		super(name, telefonnummer, jahresbetrag);
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

}
