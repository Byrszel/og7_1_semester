
public class Trainer extends Person {
	
	private String lizenzklasse;
	private int aufwandentschaedigung;
	
	public Trainer(String name, int telefonnummer, boolean jahresbetrag, String lizenzklasse,
			int aufwandentschaedigung) {
		super(name, telefonnummer, jahresbetrag);
		this.lizenzklasse = lizenzklasse;
		this.aufwandentschaedigung = aufwandentschaedigung;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(String lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getAufwandentschaedigung() {
		return aufwandentschaedigung;
	}

	public void setAufwandentschaedigung(int aufwandentschaedigung) {
		this.aufwandentschaedigung = aufwandentschaedigung;
	}

}
