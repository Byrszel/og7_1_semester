package bubble;

import java.util.Arrays;

public class BubbleSort {

	public static int[] bubblesortSortierer(int[] A) {
		for (int i = 1; i < A.length; i++) {
			for (int j = 0; j < A.length - i; j++) {
				if (A[j] > A[j + i]) {
					int temp = A[j];
					A[j] = A[j + i];
					A[j + i] = temp;
				}
			}
		}
		return A;


	}

	public static void main(String[] args) {

		int[] liste = new int[] { 3, 2, 1, 0, 5, 4, 9, 6 };
		System.out.println("Eingegebene Liste:");
		System.out.println();
		System.out.println(Arrays.toString(liste));
		System.out.println();

		System.out.println("--- Sortierung ---");
		System.out.println();
		BubbleSort.bubblesortSortierer(liste);
		for (int i = 0; i < liste.length; i++) {
			System.out.print(liste[i]);
			System.out.print(" | ");
		}

	}
}
