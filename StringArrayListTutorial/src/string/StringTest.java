package string;
import java.util.ArrayList;

public class StringTest {
	
	public static void main(String[] args) {
		ArrayList<String> Woerter = new ArrayList<String>();
		Woerter.add("String");
		Woerter.add("Array");
		Woerter.add("Int");
		Woerter.add("Boolean");
		Woerter.add("Char");
		Woerter.add("Double");
		Woerter.add("Float");
		System.out.println(Woerter.toString());
		int ergebnis = StringTest.lineareSuche(Woerter, "Int");
		if (ergebnis > 0) {
			System.out.println("Stelle: " + ergebnis);
			StringTest.bubblesortSortierer(Woerter);
			System.out.println(Woerter.toString());
		}
		else {
			System.out.println("Nicht vorhanden");
		}
		
	}

	public static int lineareSuche(ArrayList<String> liste, String gesuchtesString) {
		for (int i = 0; i < liste.size(); i++) {
			if (liste.get(i).equalsIgnoreCase(gesuchtesString)) {
				return i;
			}
		}
		return -1;
	}	
	
	public static ArrayList<String> bubblesortSortierer(ArrayList<String> liste){
        for (int i = 0; i < liste.size() - 1; i++) {
            for (int j = 0; i < liste.size() - j -1; i++) {
                if (liste.get(i + 1).compareTo(liste.get(i)) < 0) {
                    String temp = liste.get(i);
                    liste.set(i, liste.get(i + 1));
                    liste.set(i + 1, temp);
                }
            }
        }
        return liste;
	}
}