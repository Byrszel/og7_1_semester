package omnom;

public class Haustier {

	private String name;
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;

	
	
	public Haustier(String haustiername) {
		this.name = haustiername;
		this.hunger = 90;
		this.muede = 80;
		this.zufrieden = 70;
		this.gesund = 60;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger > 100)
			hunger = 100;
		if (hunger < 0)
			hunger = 0;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		this.muede = muede;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		this.zufrieden = zufrieden;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		this.gesund = gesund;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void fuettern(int anzahl) {
		this.hunger = this.getHunger() + anzahl;
		
	}

	public void schlafen(int anzahl) {
		this.muede = this.getMuede() + anzahl;
		
	}

	public void spielen(int anzahl) {
		this.zufrieden = this.getZufrieden() + anzahl;
 		
	}

	public void heilen() {
		this.setGesund(100);
	}
		
	}
		
	
