import java.util.Scanner;
import java.io.*;

public class FileControl {

	private String datum;
	private String fach;
	private String beschreibung;
	private int dauer;

	public FileControl(String datum, String fach, String beschreibung, int dauer) {
		super();
		this.datum = datum;
		this.fach = fach;
		this.beschreibung = beschreibung;
		this.dauer = dauer;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

	public void dateiEinlesenUndAusgeben(File file) throws IOException {
		//implementation
	}

	public static void main(String args[]) throws IOException {
		
		FileReader fr = new FileReader("miriam.txt");
		BufferedReader br = new BufferedReader(fr);
		String s = br.readLine();
		while (s != null)
					do {
						System.out.println(s);
						s = br.readLine();
					} while (s != null);
				br.close();
		
	}

}
