package git_taschenrechner;

import java.util.Scanner;

/**
 * @author Dennis B�rschel
 *
 */

public class TaschenrechnerTest {

	private static Scanner myScanner;

	public static void main(String[] args) {

		myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;

		// Display menu graphics
		System.out.println("============================");
		System.out.println("|         Men�auswahl      |");
		System.out.println("============================");
		System.out.println("| Optionen:                |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Multiplizieren |");
		System.out.println("|        4. Dividieren     |");
		System.out.println("|        5. Verlassen      |");
		System.out.println("============================");
		System.out.print(" Option ausw�hlen: ");
		swValue = myScanner.next().charAt(0);
		
		if (swValue == '5') {
			System.exit(0);
		}
		
		System.out.print("Erste Zahl: ");
		double zahl1 = myScanner.nextDouble();
		System.out.print("Zweite Zahl: ");
		double zahl2 = myScanner.nextDouble();
		
		// Switch construct
		switch (swValue) {
		case '1':
			System.out.println(zahl1 + " + " + zahl2 + " = " + ts.add(zahl1, zahl2));
			break;
		case '2':
			System.out.println(zahl1 + " - " + zahl2 + " = " + ts.sub(zahl1, zahl2));
			break;
		case '3':
			System.out.println(zahl1 + " * " + zahl2 + " = " + ts.mul(zahl1, zahl2));
			break;
		case '4':
			System.out.println(zahl1 + " / " + zahl2 + " = " + ts.div(zahl1, zahl2));
			break;
		case '5':
			System.exit(0);
			break;
		  
		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}

	}

}
